// Package wrappers provides tools to wrap http.handlerFuncs
package wrappers

import "net/http"

// Wrapper is an http handler function decorater
type Wrapper func(http.HandlerFunc) http.HandlerFunc

// Wrap an http.HandlerFunc with 0 or more Wrappers.
// The order of the wrappers is from outermost to innermost wrapper, or the
// order in which they will be called on a request.
func Wrap(handler http.HandlerFunc, wrappers ...Wrapper) http.HandlerFunc {

	if len(wrappers) == 0 {
		return handler
	}

	for i := len(wrappers); i > 0; i-- {
		handler = wrappers[i-1](handler)
	}

	return handler

}
