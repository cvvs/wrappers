package wrappers

import (
	"context"
	"net/http"

	"github.com/google/uuid"
)

const (
	// TIDContextKey is the key used to store a TID in a context. See https://blog.golang.org/context
	TIDContextKey contextKey = 0
)

var (
	// TransIDHeader is the header name for transaction IDs.
	TransIDHeader = "X-SITE-TRANS-ID"
)

// contextKey is used to store TID's in a context.
type contextKey int

// TIDWrapper creates a transaction ID for any request if not set.
// Generaly, this should be the first decorator applied
func TIDWrapper(next http.HandlerFunc) http.HandlerFunc {

	fn := func(rw http.ResponseWriter, req *http.Request) {
		tid := req.Header.Get(TransIDHeader)
		if tid == "" {
			// let's hope this doesn't error
			uid := uuid.New()
			tid = uid.String()
			req.Header.Set(TransIDHeader, tid)
		}
		// Add the TID to the context
		req = req.WithContext(context.WithValue(req.Context(), TIDContextKey, tid))
		rw.Header().Set(TransIDHeader, tid)
		next(rw, req)
	}

	return fn
}
