package wrappers

import (
	"net/http"
)

// NewContentTypeWrapper returns a wrapper which sets the content type for the request
func NewContentTypeWrapper(contentType string) Wrapper {
	wrap := func(next http.HandlerFunc) http.HandlerFunc {
		fn := func(rw http.ResponseWriter, req *http.Request) {
			rw.Header().Set("Content-Type", contentType)
			next(rw, req)
		}
		return fn
	}
	return wrap
}
