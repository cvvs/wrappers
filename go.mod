module bitbucket.org/cvvs/wrappers

go 1.15

require (
	bitbucket.org/cvvs/auth v0.0.0-20181106152737-b36f192d3a07
	github.com/google/uuid v1.1.2
)
