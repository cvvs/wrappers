# wrappers

Yet another Go http decorator package. This time with feeling.

**Requires go.uuid v1.2.0**

    ...
    authenwrapper := wrappers.NewAuthenticationWrapper([]auth.Authenticator{authenticate})
	jsonwrapper := wrappers.NewContentTypeWrapper("application/json")
	htmlwrapper := wrappers.NewContentTypeWrapper("text/html")
	tidwrapper := wrappers.TIDWrapper

	apiPost := wrappers.Wrap(api.PostHandler(), tidwrapper, authenwrapper, jsonwrapper)
	root := wrappers.Wrap(handlers.StaticHandler(wwwRoot), tidwrapper, htmlwrapper)

	m := http.NewServeMux()
	m.HandleFunc("/", root)
	m.HandleFunc("/api", apiPost)
	
	log.Print(http.ListenAndServe(":80", m))
    ...
