package wrappers

import (
	"context"
	"net/http"

	"bitbucket.org/cvvs/auth"
)

const (
	// AuthorizorContextKey is the key used to store and retrieve an authorizor in the context
	AuthorizorContextKey authorizorContextKey = 1
)

type authorizorContextKey int

// NewAuthenticationWrapper creates a wrapper which will authenticate an endpoint using the supplied authenticators
func NewAuthenticationWrapper(authens []auth.Authenticator) Wrapper {

	wrapped := func(next http.HandlerFunc) http.HandlerFunc {

		f := func(rw http.ResponseWriter, req *http.Request) {
			for i := range authens {
				if err := authens[i].Authenticate(req); err == nil {
					ctx := req.Context()
					ctx = context.WithValue(ctx, AuthorizorContextKey, authens[i].Authorizor())
					req = req.WithContext(ctx)
					next(rw, req)
					return
				}
			}
			http.Error(rw, "401 Unauthorized", http.StatusUnauthorized)
		}
		return f

	}

	return wrapped
}

// AuthorizationWrapper is a wrapper which will authorize requests on the provided endpoint
func AuthorizationWrapper(next http.HandlerFunc) http.HandlerFunc {

	fn := func(rw http.ResponseWriter, req *http.Request) {

		ctx := req.Context()
		val := ctx.Value(AuthorizorContextKey)

		auth, ok := val.(auth.Authorizor)
		if ok {
			if err := auth.Authorize(req); err == nil {
				next(rw, req)
				return
			}
		}

		http.Error(rw, "403 Forbidden", http.StatusForbidden)
	}

	return fn

}
